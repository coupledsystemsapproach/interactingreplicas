#!/bin/bash
#$ -S /bin/bash
#$ -cwd
#$ -pe ompi 12
#$ -q comp.q
#$ -P prabha_group
#$ -V
#rm -rf /scratch/abhirath/
rm -rf /scratch/abhirath/e4
mkdir -p /scratch/abhirath/e4/
for i in `seq 0 19`
do
cp -r $i /scratch/abhirath/e4/
done
cp code.py /scratch/abhirath/e4/
cp prod.mdp topol.top /scratch/abhirath/e4/
cd /scratch/abhirath/e4/
#e4xport GMX_MAXBACKUP=2000
python code.py 
cp ~/extract.sh .
sh extract.sh
mkdir -p ~/prep/e4/result/
cp colvar_* ~/prep/e4/result/
