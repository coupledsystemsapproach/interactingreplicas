import math, sys, time
import dispy
import os
import subprocess
import numpy as np
import re

import time
import multiprocessing as mp
try:
    from cStringIO import StringIO
except ImportError:
    from StringIO import StringIO


def set_k(f,k): #sets the value of lambda(k) in a plumed dat file string
    return re.sub('dis([0-9]+)cs(.*)COEFFICIENTS=[0-9\.-]+',r'dis\1cs\2COEFFICIENTS=%.2f'%k,f)
def spawn_sim(i,it,steps): #starts a new simulation for a given replica, given iteration and given number of steps
    os.chdir('%d'%i) #enters the directory of the replica
    if it>0:
        os.mkdir('%d'%it)  #create a directory for the iteration number
        os.system('cp %d/prod%d.tpr %d/plumed.dat %d/'%(it-1,i,it-1,it))  #get a modified plumed config file and correct tpr file
    os.chdir('%d'%it) #enter the iteration directory
    proc = subprocess.Popen(['mdrun_mpi','-deffnm','prod%d.tpr'%i,'-plumed','plumed.dat','-nsteps',"%d"%steps],stdout=subprocess.PIPE,stderr=subprocess.PIPE,close_fds=True)  #spawn an OS process with gromacs
    sto,ste = proc.communicate()
    os.chdir('../../') # return to parent directory
    return 1


def _trjconv(step,i,it): #runs trjconv from the gromacs suite to pull out a frame from the trajectory into a conf file
    writeStdin=StringIO()
    trajfile='prod%d.tpr.xtc'%(i)
    tprfile='prod%d.tpr'%i
    grooutname="out%d.gro"%i
    cmdline = ['trjconv_mpi'] + ['-s', tprfile, '-f', trajfile]
    cmdline.extend(['-o', grooutname])
    cmdline.extend(['-dump', "%g"%step] )
    writeStdin.write("System\n")
    proc=subprocess.Popen(cmdline,
                          stdin=subprocess.PIPE,
                          stdout=subprocess.PIPE,
                          stderr=subprocess.STDOUT,
                          cwd='%d/%d'%(i,it),
                          close_fds=True)
    (stdout, stderr)=proc.communicate(writeStdin.getvalue())
    proc.stdout.close()
#proc.stderr.close()
    proc.stdin.close()
    if proc.returncode != 0:
        print "Well. Shit! : %s"%stdout

    #grompp_mpi command uses this extracted frame to then create a new tpr file for the next iteration

    cmdlist = ['grompp_mpi']  + ["-f", '../../prod.mdp', "-quiet",
                                          "-p", '../../topol.top',
                                          "-c", grooutname,
                                          "-t","prod%d.tpr.cpt"%i,
                                          "-o",  tprfile]
    proc2=subprocess.Popen(cmdlist,
                          stdin=subprocess.PIPE,
                          stdout=subprocess.PIPE,
                          stderr=subprocess.STDOUT,
                          cwd="%d/%d"%(i,it),
                          close_fds=True)
    
    (stdout, stderr)=proc2.communicate()
    proc2.stdout.close()
#    proc2.stderr.close()
    
    proc2.stdin.close()
    if proc2.returncode != 0:
        print "Tch : %s"%stdout

num = 20  #number of replicas
step = 0.2  #length of one stage
total = 1000  #total number of stages
alphas = 'abcdefghijklmnopqrstuvwxyz'  #plumed config file is written such that each collective variable is represented as a letter.  
ncpus = 20 #number of cpus to allot to the job

# loop into this 
for iteration in range(total):
    print 'Entering iteration %d'%iteration 
    sys.stdout.flush()
    jobs = []
    start_time = time.time()
    nsteps = 2500 if iteration==0 else 100
    #spawn jobs with the multiprocess library
    for i in range(num):
        proc = mp.Process(target=spawn_sim,args=(i,iteration,nsteps))
        proc.start()
        
        jobs.append(proc)
    for job in jobs:
        job.join()
    end_time = time.time()
    print "Time taken:",end_time-start_time
    cvs = []
    #collect CV data
    for i in range(num):
        fi = open('%d/%d/COLVAR'%(i,iteration)) 
        f = fi.read().split('\n')[:-1][-1]
        fi.close()
        f = f.split(' ')
        f = [float(j) for j in f[4:]]
        cvs.append(f)
    l = len(cvs[1])
 #   raw_input("CV files closed")
    for i in xrange(num): #for each replica
        vec = np.zeros((l,))
        pl = open('%d/%d/plumed.dat'%(i,iteration),'r')
        plumed_dat = pl.read() #read data
        pl.close()
        pl = open('%d/%d/plumed.dat'%(i,iteration),'w')
        delt = 1
        for j in xrange(num):# loop to write values of CVs of other files as coefficients in the plumed config file
            if j==i:
                delt = 0
                continue 
            for k in xrange(l):
                rgx = '\n%s%d(.*)COEFFICIENTS=[0-9\.-]+'%(alphas[k],j+delt)
                rpl = r'\n%s%d\1COEFFICIENTS=%f'%(alphas[k],j+delt,cvs[j][k])
                plumed_dat = re.sub(rgx,rpl,plumed_dat)  #put the changes in the file string

        kval = (iteration/4) * (100.0/500) if iteration <= 500 else 25
        plumed_dat = set_k(plumed_dat,kval)
        print "iteration:",iteration," k:",kval

        pl.write(plumed_dat) #write changes to file
        pl.close()
        _trjconv(step,i,iteration)
print "Ho gaya"
