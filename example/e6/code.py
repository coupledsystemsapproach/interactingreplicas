import math, sys, time
import dispy
import os
import subprocess
import numpy as np
import re

import time
import multiprocessing as mp
try:
    from cStringIO import StringIO
except ImportError:
    from StringIO import StringIO


def set_k(f,k):
    return re.sub('dis([0-9]+)cs(.*)COEFFICIENTS=[0-9\.-]+',r'dis\1cs\2COEFFICIENTS=%.2f'%k,f)
def spawn_sim(i,it,steps):
    os.chdir('%d'%i)
    if it>0:
        os.mkdir('%d'%it)
        os.system('cp %d/prod%d.tpr %d/plumed.dat %d/'%(it-1,i,it-1,it))
    os.chdir('%d'%it)                                                 
    proc = subprocess.Popen(['mdrun_mpi','-deffnm','prod%d.tpr'%i,'-plumed','plumed.dat','-nsteps',"%d"%steps],stdout=subprocess.PIPE,stderr=subprocess.PIPE,close_fds=True)
    sto,ste = proc.communicate()
    os.chdir('../../')
    return 1


def _trjconv(step,i,it):
    writeStdin=StringIO()
    trajfile='prod%d.tpr.xtc'%(i)
    tprfile='prod%d.tpr'%i
    grooutname="out%d.gro"%i
    cmdline = ['trjconv_mpi'] + ['-s', tprfile, '-f', trajfile]
    cmdline.extend(['-o', grooutname])
    cmdline.extend(['-dump', "%g"%step] )
    writeStdin.write("System\n")
    proc=subprocess.Popen(cmdline,
                          stdin=subprocess.PIPE,
                          stdout=subprocess.PIPE,
                          stderr=subprocess.STDOUT,
                          cwd='%d/%d'%(i,it),
                          close_fds=True)
    (stdout, stderr)=proc.communicate(writeStdin.getvalue())
    proc.stdout.close()
#proc.stderr.close()
    proc.stdin.close()
    if proc.returncode != 0:
        print "Well. Shit! : %s"%stdout
    cmdlist = ['grompp_mpi']  + ["-f", '../../prod.mdp', "-quiet",
                                          "-p", '../../topol.top',
                                          "-c", grooutname,
                                          "-t","prod%d.tpr.cpt"%i,
                                          "-o",  tprfile]
    proc2=subprocess.Popen(cmdlist,
                          stdin=subprocess.PIPE,
                          stdout=subprocess.PIPE,
                          stderr=subprocess.STDOUT,
                          cwd="%d/%d"%(i,it),
                          close_fds=True)
    
    (stdout, stderr)=proc2.communicate()
    proc2.stdout.close()
#    proc2.stderr.close()
    
    proc2.stdin.close()
    if proc2.returncode != 0:
        print "Tch : %s"%stdout

num = 20
step = 0.2
total = 1000
alphas = 'abcdefghijklmnopqrstuvwxyz'
ncpus = 20

# loop into this 
for iteration in range(total):
    print 'Entering iteration %d'%iteration 
    sys.stdout.flush()
    jobs = []
    start_time = time.time()
    nsteps = 2500 if iteration==0 else 100
    for i in range(num):
        proc = mp.Process(target=spawn_sim,args=(i,iteration,nsteps))
        proc.start()
        
        jobs.append(proc)
    for job in jobs:
        job.join()
    end_time = time.time()
    print "Time taken:",end_time-start_time
    cvs = []
    for i in range(num):
        fi = open('%d/%d/COLVAR'%(i,iteration))
        f = fi.read().split('\n')[:-1][-1]
        fi.close()
        f = f.split(' ')
        f = [float(j) for j in f[4:]]
        cvs.append(f)
    l = len(cvs[1])
 #   raw_input("CV files closed")
    for i in xrange(num):
        vec = np.zeros((l,))
        pl = open('%d/%d/plumed.dat'%(i,iteration),'r')
        plumed_dat = pl.read()
        pl.close()
        pl = open('%d/%d/plumed.dat'%(i,iteration),'w')
        delt = 1
        for j in xrange(num):
            if j==i:
                delt = 0
                continue 
            for k in xrange(l):
                rgx = '\n%s%d(.*)COEFFICIENTS=[0-9\.-]+'%(alphas[k],j+delt)
                rpl = r'\n%s%d\1COEFFICIENTS=%f'%(alphas[k],j+delt,cvs[j][k])
                plumed_dat = re.sub(rgx,rpl,plumed_dat)

        kval = (iteration/4) * (100.0/500) if iteration <= 500 else 25
        plumed_dat = set_k(plumed_dat,kval)
        print "iteration:",iteration," k:",kval
        '''
        if iteration == 10:
            plumed_dat = set_k(plumed_dat,1)
        elif iteration == 20:
            plumed_dat = set_k(plumed_dat,5)
        elif iteration == 100:
            plumed_dat = set_k(plumed_dat,10)
        elif iteration == 200:
            plumed_dat = set_k(plumed_dat,15)
        elif iteration == 300:
            plumed_dat = set_k(plumed_dat,20)
        elif iteration == 500:
            plumed_dat = set_k(plumed_dat,25)
        '''

        pl.write(plumed_dat)
        pl.close()
  #      raw_input("plumed files closed")
        _trjconv(step,i,iteration)
print "Ho gaya"
