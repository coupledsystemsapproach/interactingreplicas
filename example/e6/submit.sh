#!/bin/bash
#$ -S /bin/bash
#$ -cwd
#$ -pe ompi 12
#$ -q comp.q
#$ -P prabha_group
#$ -V
#rm -rf /scratch/abhirath/
rm -rf /scratch/abhirath/e9
mkdir -p /scratch/abhirath/e9/
for i in `seq 0 19`
do
cp -r $i /scratch/abhirath/e9/
done
cp code.py /scratch/abhirath/e9/
cp prod.mdp topol.top /scratch/abhirath/e9/
cd /scratch/abhirath/e9/
#e9xport GMX_MAXBACKUP=2000
python code.py 
cp ~/extract.sh .
sh extract.sh
mkdir -p ~/prep/e9/result/
cp colvar_* ~/prep/e9/result/
