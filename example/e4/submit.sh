#!/bin/bash
#$ -S /bin/bash
#$ -cwd
#$ -pe ompi 12
#$ -q comp.q
#$ -P prabha_group
#$ -V
#rm -rf /scratch/abhirath/
rm -rf /scratch/abhirath/e7
mkdir -p /scratch/abhirath/e7/
for i in `seq 0 19`
do
cp -r $i /scratch/abhirath/e7/
done
cp code.py /scratch/abhirath/e7/
cp prod.mdp topol.top /scratch/abhirath/e7/
cd /scratch/abhirath/e7/
#e7xport GMX_MAXBACKUP=2000
python code.py 
cp ~/extract.sh .
sh extract.sh
mkdir -p ~/prep/e7/result/
cp colvar_* ~/prep/e7/result/
