#!/bin/bash
#$ -S /bin/bash
#$ -cwd
#$ -pe ompi 12
#$ -q comp.q
#$ -P prabha_group
#$ -V
#rm -rf /scratch/abhirath/
rm -rf /scratch/abhirath/e6
mkdir -p /scratch/abhirath/e6/
for i in `seq 0 19`
do
cp -r $i /scratch/abhirath/e6/
done
cp code.py /scratch/abhirath/e6/
cp prod.mdp topol.top /scratch/abhirath/e6/
cd /scratch/abhirath/e6/
#e6xport GMX_MAXBACKUP=2000
python code.py 
cp ~/extract.sh .
sh extract.sh
mkdir -p ~/prep/e6/result/
cp colvar_* ~/prep/e6/result/
