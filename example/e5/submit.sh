#!/bin/bash
#$ -S /bin/bash
#$ -cwd
#$ -pe ompi 12
#$ -q comp.q
#$ -P prabha_group
#$ -V
#rm -rf /scratch/abhirath/
rm -rf /scratch/abhirath/e8
mkdir -p /scratch/abhirath/e8/
for i in `seq 0 19`
do
cp -r $i /scratch/abhirath/e8/
done
cp code.py /scratch/abhirath/e8/
cp prod.mdp topol.top /scratch/abhirath/e8/
cd /scratch/abhirath/e8/
#e8xport GMX_MAXBACKUP=2000
python code.py 
cp ~/extract.sh .
sh extract.sh
mkdir -p ~/prep/e8/result/
cp colvar_* ~/prep/e8/result/
