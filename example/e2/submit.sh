#!/bin/bash
#$ -S /bin/bash
#$ -cwd
#$ -pe ompi 12
#$ -q comp.q
#$ -P prabha_group
#$ -V
#rm -rf /scratch/abhirath/
rm -rf /scratch/abhirath/e5
mkdir -p /scratch/abhirath/e5/
for i in `seq 0 19`
do
cp -r $i /scratch/abhirath/e5/
done
cp code.py /scratch/abhirath/e5/
cp prod.mdp topol.top /scratch/abhirath/e5/
cd /scratch/abhirath/e5/
#e5xport GMX_MAXBACKUP=2000
python code.py 
cp ~/extract.sh .
sh extract.sh
mkdir -p ~/prep/e5/result/
cp colvar_* ~/prep/e5/result/
