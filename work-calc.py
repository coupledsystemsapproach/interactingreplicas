import numpy as np
from matplotlib.pylab import *
import bigfloat as bf
c =[] 
beta = 1/2.495
for i in range(20):
    c.append(np.loadtxt('result_2-9/colvar_%d'%i))
trajs = []
for i in range(20):
    trajs.append([])
    t = w0 = be = bs = 0
    for j in range(len(c[i])):
        if c[i][j][0] == 0.0 and j!=0:
            w0 += c[i][j][2] - c[i][j-1][2]
        trajs[i].append([c[i][j][3],c[i][j][4],c[i][j][1],w0])
size = 100
bins = np.zeros((size+1,size+1))
nums = 0.0
k = 0
total_time = 11000
np.seterr(over='print',divide='print',invalid='print')
for i in range(total_time):
    dn = 0
    W_ik = np.array([trajs[k][i][3] for k in range(20)])
    eta_i = 0
    mx = np.max(W_ik)
    mn = np.min(W_ik)
    avg = (mx + mn)/2
    for w in W_ik:

        eta_i += (bf.exp(-1 * beta * (w - avg),bf.precision(100)))/20
        #print w-avg,
   # print eta_i
    
    for j in range(20):
        phi = trajs[j][i][0]
        psi = trajs[j][i][1]
        W_i = trajs[j][i][3]
        phi_p = int(((phi+3.14)/(2*3.14))*size)
        psi_p = int(((psi+3.14)/(2*3.14))*size)
        
        bins[phi_p][psi_p] += bf.exp(-1*beta*(W_i-avg),bf.precision(100))/(eta_i*20)
    nums += bf.exp(trajs[0][i][2])/eta_i
#print maxs
#print nums
bins = bins/nums
for i in range(size+1):
    for j in range(size+1):
        if bins[i][j]!=0:
            bins[i][j] = bf.log(bins[i][j])
        else:
            bins[i][j] = -10

bins = -1 * bins  *beta
np.savetxt("fe",bins)
